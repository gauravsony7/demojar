@PIXL-2448 @all
@GuestUser

Feature: Guest customers with promo code

  Scenario: Guest customers with email not subscribed can not enter subscription specific promo code
    And Select product and add it to cart
    And Apply promo code
    And  Provide email address and click on continue
    Then Verify error message


