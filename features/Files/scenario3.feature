@PIXL-317 @all
@GuestUser
Feature: Place an pre order with Guest customer

  Scenario: Order placement for staff free pair when changing between free and paid delivery methods
    Given Select product and add it to cart
    And Apply ALLOFF promo code
    And Provide email address and personal information and click on continue
    Then Change delivery method to paid delivery
    Then Fill payment information
    Then Change delivery method to free delivery and confirm order




