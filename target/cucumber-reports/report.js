$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("scenario2.feature");
formatter.feature({
  "line": 4,
  "name": "Guest customers with promo code",
  "description": "",
  "id": "guest-customers-with-promo-code",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@PIXL-2448"
    },
    {
      "line": 1,
      "name": "@all"
    },
    {
      "line": 2,
      "name": "@GuestUser"
    }
  ]
});
formatter.before({
  "duration": 63074657543,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Guest customers with email not subscribed can not enter subscription specific promo code",
  "description": "",
  "id": "guest-customers-with-promo-code;guest-customers-with-email-not-subscribed-can-not-enter-subscription-specific-promo-code",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "Select product and add it to cart",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Apply promo code",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Provide email address and click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Verify error message",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefOfScenario1.iAddChooseProductAndAddItToCart()"
});
formatter.result({
  "duration": 288552309,
  "error_message": "java.lang.NoSuchMethodError: org.openqa.selenium.support.ui.WebDriverWait.until(Ljava/util/function/Function;)Ljava/lang/Object;\r\n\tat com.pmcretail.driver.DriverUtil.waitUntilClickable(DriverUtil.java:265)\r\n\tat com.Pages.HomePage.selectCategoryUnderWomen(HomePage.java:42)\r\n\tat com.StepDefination.StepDefOfScenario1.iAddChooseProductAndAddItToCart(StepDefOfScenario1.java:50)\r\n\tat ✽.And Select product and add it to cart(scenario2.feature:7)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "StepDefOfScenario2.iApplyPromoCode()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "StepDefOfScenario2.iCompleteCheckoutUsingPersonalAndPaymentDetail()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "StepDefOfScenario2.iVerifyErrorMessage()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1726788344,
  "status": "passed"
});
});