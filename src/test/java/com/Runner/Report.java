package com.Runner;

import com.pmcretail.readwritedata.PropertyUtil;
import com.pmcretail.reporting.ReportGenerator;
import com.pmcretail.reporting.jsonReportFormatter;
import org.apache.commons.io.FilenameUtils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Report {

    public static void main(String[] args){

        jsonReportFormatter.formatJsonFile();


        ReportGenerator reporter = new ReportGenerator();
        reporter.GenerateCucumberReport(System.getProperty("user.dir") + "/Report", "FitFlop", System.getProperty("user.dir") + "/target/cucumber", "Windows", "Chrome");
        //reporter.GenerateCucumberReport("E:\\PMCAutomation\\kesha", "THL",  "E:\\PMCAutomation\\kesha");
        //reporter.GenerateCucumberReport("D:\\PMCAutomation\\THL Automation\\target\\cucumber", "THL", "D:\\PMCAutomation\\THL Automation\\target\\cucumber", "Windows", "Chrome");


        try {

            PropertyUtil property = new PropertyUtil();

            if (property.getProperty("ShowReportInBrowser").equalsIgnoreCase("yes")) {
                File htmlFile = new File(property.getProperty("reportLocation"));
                Desktop.getDesktop().browse(htmlFile.toURI());
            }


            String command= getCommandTOPushResults();

            System.out.println("command " + command);

            Process process = Runtime.getRuntime().exec(command);


        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static String getCommandTOPushResults()
    {
        String command = "curl -X POST";

        File jsonFile = new File(System.getProperty("user.dir") + "/target/cucumber");
        List<String> jsonFiles = new ArrayList();
        File[] var9 = jsonFile.listFiles();
        int var10 = var9.length;

        for (int var11 = 0; var11 < var10; ++var11) {
            File cucufile = var9[var11];
            if (cucufile.isFile() && cucufile.getName().contains("Cucumber") && FilenameUtils.getExtension(cucufile.getName().toString()).equalsIgnoreCase("json"))
            {
                jsonFiles.add(cucufile.getAbsoluteFile().getAbsolutePath());
                System.out.println(cucufile.getAbsoluteFile().getAbsolutePath());
                command=command+" -F file=@"+cucufile.getAbsoluteFile().getAbsolutePath();
            }
        }
        return command+" https://app.hiptest.com/import_test_reports/401132124770629898611648629952727579513931751826456744459/253231/cucumber-json";
    }


}
