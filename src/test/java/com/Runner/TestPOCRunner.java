package com.Runner;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumberEx5.json",
        retryCount = 0)

@CucumberOptions(monochrome = true, format = {"json", "html:target/cucumber-reports",
        "json:target/cucumber/CucumberALL.json"},
        tags = {"@PIXL-2448","~@ex"},
        glue = {"com.StepDefination"},
        dryRun = false,
        features = "features/Files")
//features = "Features/StagePackFeature")
public class TestPOCRunner {

}