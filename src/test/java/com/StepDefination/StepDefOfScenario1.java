package com.StepDefination;

import com.Pages.*;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;


public class StepDefOfScenario1
{
    WebDriver driver;
    private ReadWriteExcelFile objExcel = new ReadWriteExcelFile();
    HomePage homePage;
    CataloguePage catlogPage;
    ProductDetailPage pdp;
    CartPage cartPage;
    CheckOutPage checkoutPage;
    ConfirmationPage cpage;
    RegistrationPage rPage;
    OrderReviewsPage orPage;
    static Logger logger = Logger.getLogger("Step Defination of scenario 1");

    public StepDefOfScenario1()
    {
        try {
            this.driver = DriverUtil.getWebDriver();
            homePage = new HomePage();
            catlogPage =new CataloguePage();
            pdp=new ProductDetailPage();
            cartPage = new CartPage();
            checkoutPage = new CheckOutPage();
            cpage= new ConfirmationPage();
            rPage= new RegistrationPage();
            orPage= new OrderReviewsPage();

            System.out.println("Driver inside step def :"+driver);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Given("^Select product and add it to cart$")
    public void iAddChooseProductAndAddItToCart() {

        homePage.selectCategoryUnderWomen();
        catlogPage.clickONproduct();
        pdp.addProductToCart();
        cartPage.waitForOrderSummaryPage();
    }


    @Then("^Complete checkout using personal and payment detail$")
    public void iCompleteCheckoutUsingPersonalAndPaymentDetail() {
        cartPage.clickOnCheckout();
        checkoutPage.completeCheckout();
        orPage.clickOnPlaceOrderbtn();
    }


    @Then("^Confirm order and compete registration$")
    public void iConfirmOrder() {

        cpage.clickOnCreateAccountLink();
        rPage.completeRegisration();

    }
}
