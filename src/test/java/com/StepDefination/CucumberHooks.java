package com.StepDefination;


import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.PropertyUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by solankia on 6/6/2017. Modified by Suprabha on 7/6/2017.
 */
public class CucumberHooks {

    public static Scenario scenario = null;
    private static PropertyUtil property = new PropertyUtil();
    private ReadWriteExcelFile readData = new ReadWriteExcelFile();
    private static WebDriver driver;
    static Logger logger = Logger.getLogger("CucumberHooks");

    static {
        System.setProperty("baseUrl", property.getProperty("env.baseurl"));
    }

    /**
     * This method is called before each scenario
     */
    @Before(order = 0)
    public void openBrowser(Scenario scenario) {
        this.scenario = scenario;
        try {

            System.out.println("Before scenario started");
            DriverUtil.initDriver();
            driver = DriverUtil.getWebDriver();
            BasePage.timeout(1);
            driver.manage().window().setSize(new Dimension(1366, 768));
            logger.info("Driver  Size " + driver.manage().window().getSize());

            Thread.sleep(1000);
            DriverUtil.loadUrl(property.getProperty("env.baseurl"));
            Thread.sleep(2000);
           //  DriverUtil.waitForJStoLoad();
            DriverUtil.getWebDriver().manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            DriverUtil.getWebDriver().manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
            System.out.println("Before scenario finished");

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


    /**
     * This method will be called after each scenario, it will take screen shot
     * if test case is failed
     *
     * @param scenario
     */
    @After(order = 0)
    public void closeBrowser(Scenario scenario) {

        if (scenario.isFailed()) {

            final byte[] screenshot = ((TakesScreenshot) DriverUtil
                    .getWebDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // stick it in the report

            logger.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            logger.error("!!!!!!!!!!!!! The test " + scenario.getName() + " is Failed !!!!!!!!!!!!!!!!!");
            logger.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        } else {
            logger.info("*************************************************************************");
            logger.info("************* The test " + scenario.getName() + " is Passed *****************");
            logger.info("*************************************************************************");
        }
        DriverUtil.quit();
    }


    /**
     * This method will write data in Cucumber Report
     *
     * @param data
     */
    public void writeInReport(String data) {

        scenario.write(data);
    }
}
