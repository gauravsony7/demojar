package com.StepDefination;

import com.Pages.*;
import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class StepDefOfScenario3 {



    HomePage homePage;
    CataloguePage catlogPage;
    ProductDetailPage pdp;
    CartPage cartPage;
    CheckOutPage checkoutPage;
    ConfirmationPage cpage;
    RegistrationPage rPage;
    OrderReviewsPage orPage;
    static Logger logger = Logger.getLogger("Step Defination of scenario 1");

    WebDriver driver;
    CheckOutPage chkOutPage;

    private ReadWriteExcelFile objExcel = new ReadWriteExcelFile();

    public StepDefOfScenario3() {
        try {
            logger.info("Before class");
            this.driver = DriverUtil.getWebDriver();
            cartPage = new CartPage();
            this.driver = DriverUtil.getWebDriver();
            homePage = new HomePage();
            catlogPage =new CataloguePage();
            pdp=new ProductDetailPage();
            cartPage = new CartPage();
            checkoutPage = new CheckOutPage();
            cpage= new ConfirmationPage();
            rPage= new RegistrationPage();
            orPage= new OrderReviewsPage();

            System.out.println("Driver inside step def :"+driver);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @And("^Provide email address and personal information and click on continue$")
    public void provideEmailAddressAndPersonalInformationAndClickOnContinue() {
        cartPage.clickOnCheckout();
        checkoutPage.fillEmailAddressa();
        checkoutPage.fillPersonalInfo();
    }

    @Then("^Change delivery method to paid delivery$")
    public void changeDeliveryMethod() {
        orPage.changeDelivery("paid");
    }


    @Then("^Fill payment information$")
    public void fillPaymentInformation() {
        DriverUtil.waitForJStoLoad();
        DriverUtil.getWebDriver().navigate().refresh();
        DriverUtil.waitForJStoLoad();
        checkoutPage.choosePaymentMethod();
    }

    @Then("^Change delivery method to free delivery and confirm order$")
    public void iConfirmOrder() {
        orPage.changeDelivery("free");
        BasePage.timeout(3000);
        orPage.clickOnPlaceOrderbtn();
        cpage.waitforCreateAccountLink();
    }

    @And("^Apply ALLOFF promo code$")
    public void iApplyallPromoCode() {
        cartPage.applyPromocode("ALLOFF");
    }

}
