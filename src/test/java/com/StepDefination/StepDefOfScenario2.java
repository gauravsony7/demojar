package com.StepDefination;

import com.Pages.*;
import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefOfScenario2 {

    WebDriver driver;
    private ReadWriteExcelFile objExcel = new ReadWriteExcelFile();
    HomePage homePage;
    LoginPage loginPage;
    CataloguePage catlogPage;
    ProductDetailPage pdp;
    CartPage cartPage;
    CheckOutPage checkoutPage;
    ConfirmationPage cpage;
    RegistrationPage rPage;
    OrderReviewsPage orPage;
    static Logger logger = Logger.getLogger("Step Defination of scenario 1");
    CheckOutPage chkOutPage;

    public StepDefOfScenario2() {
        try {
            logger.info("Before class");
            this.driver = DriverUtil.getWebDriver();
            homePage = new HomePage();
            loginPage= new LoginPage();

            this.driver = DriverUtil.getWebDriver();
            cartPage = new CartPage();
            this.driver = DriverUtil.getWebDriver();
            homePage = new HomePage();
            catlogPage =new CataloguePage();
            pdp=new ProductDetailPage();
            cartPage = new CartPage();
            checkoutPage = new CheckOutPage();
            cpage= new ConfirmationPage();
            rPage= new RegistrationPage();
            orPage= new OrderReviewsPage();
            System.out.println("Driver inside step def :"+driver);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Given("^Login to fitflop website$")
    public void iLoginToFitflop() {

        homePage.clickSignInIcon();
        loginPage.login(objExcel.getCellValue("username"),objExcel.getCellValue("password"));

    }

    @And("^Apply promo code$")
    public void iApplyPromoCode() {
        cartPage.applyPromocode(objExcel.getCellValue("promocode"));
    }


    @Then("^Verify error message$")
    public void iVerifyErrorMessage() {
        BasePage.timeout(3000);
        Assert.assertTrue(cartPage.verifyErrorMessgeforWrongPromoCode(objExcel.getCellValue("errorMessgaeForWrongPromo")));
        BasePage.timeout(5000);
    }

    @Then("^Provide email address and click on continue$")
    public void iCompleteCheckoutUsingPersonalAndPaymentDetail() {
        cartPage.clickOnCheckout();
        checkoutPage.fillEmailAddressa();
    }



}
