package com.Util;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewReport {

    // test annotation only to make sure it is generated during "mvn test"
    public static void main(String args[]) throws IOException {
        // System.getProperty("user.dir") + "/Report"
        File reportOutputDirectory = new File("target/demo");



        File jsonFile = new File(System.getProperty("user.dir") + "/target/cucumber");
        List<String> jsonFiles = new ArrayList();
        File[] var9 = jsonFile.listFiles();
        int var10 = var9.length;

        for(int var11 = 0; var11 < var10; ++var11) {
            File cucufile = var9[var11];
            if (cucufile.isFile() && cucufile.getName().contains("Cucumber") && FilenameUtils.getExtension(cucufile.getName().toString()).equalsIgnoreCase("json")) {
                jsonFiles.add(cucufile.getAbsoluteFile().getAbsolutePath());
                System.out.println(cucufile.getAbsoluteFile().getAbsolutePath());
            }
        }
       // List<String> jsonFiles = new ArrayList<>();
        //jsonFiles.add(System.getProperty("user.dir") + "/target/cucumber/CucumberALL.json");

        String buildNumber = "106";
        String projectName = "Live Demo Project";
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        configuration.setBuildNumber(buildNumber);

        configuration.addClassifications("Browser", "Firefox");
        configuration.addClassifications("Branch", "release/1.0");
        ///configuration.setSortingMethod(SortingMethod.NATURAL);
        //configuration.addPresentationModes(PresentationMode.EXPAND_ALL_STEPS);


        File trends = new File(System.getProperty("user.dir") + "/target/cucumber/trends.json");
        configuration.setTrends(trends,10);



        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
    }
}
