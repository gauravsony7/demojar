package com.Util;

import com.pmcretail.driver.DriverUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class BasePage
{

    final private static int waitTime= 60;
    private static  JavascriptExecutor js;
    private static Logger logger;


    static
    {
        logger = Logger.getLogger("Base page logger");
    }



    public static void highLightElement(WebElement ele)
    {
        highLightElement(ele,"red");
    }
    //use executeScript() method and pass the arguments
    //Here i pass values based on css style. Yellow background color with solid red color border.
    public static void highLightElement(WebElement ele, String colour)
    {
        try
        {
            js = (JavascriptExecutor) DriverUtil.getWebDriver();
            js.executeScript("arguments[0].setAttribute('style', 'border: 2px solid "+colour+";');", ele);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method will verify if the element is present in DOM or not
     *
     * @param ele
     * @return
     */
    public static boolean isDisplayed(WebElement ele)
    {
        try
        {
            if(ele.isDisplayed())
            {
                highLightElement(ele,"blue");
                return true;
            }
            return false;
        } catch (Exception e)
        {
            return false;
        }
    }

    /**
     * This method will verify if the element is present in DOM or not
     *
     * @param ele
     * @return
     */
    public static boolean isDisplayed(By ele)
    {
        try
        {
            if(DriverUtil.getWebDriver().findElement(ele).isDisplayed())
            {
                highLightElement(DriverUtil.getWebDriver().findElement(ele),"blue");
                return true;
            }
            return false;
        } catch (Exception e)
        {
            return false;
        }
    }

    /**
     * Wait until element is visible in DOM
     *
     * @param locator
     */
    public static void waitUntilVisibilityOf(WebElement locator)
    {
        waitUntilVisibilityOf(locator,waitTime);
    }

    /**
     * Wait until element is visible in DOM
     *
     * @param locator, Seconds
     */
    public static void waitUntilVisibilityOf(WebElement locator,int seconds)
    {

        try {
            logger.info("Waiting for " + locator);
            WebDriverWait wait = new WebDriverWait(DriverUtil.getWebDriver(), seconds);
            wait.until(ExpectedConditions.visibilityOf(locator));
            highLightElement(locator);
            logger.info("Done");
        } catch (Exception e) {
            logger.info("There was an error " + e.getMessage());
        }
    }

    /**
     * This method will wait for all element array to be visible
     *
     * @param locator
     */
    public static void waitUntilVisibilityOf(List<WebElement> locator) {

        try {
            WebDriverWait wait = new WebDriverWait(DriverUtil.getWebDriver(), waitTime);
            wait.until(ExpectedConditions.visibilityOfAllElements(locator));
        } catch (Exception e) {
            logger.info("There was an error " + e.getMessage());
        }
    }

    /**
     * Wait until element is disappear
     *
     * @param locator
     */
    public static void waitUntilInVisibilityOf(WebElement locator) {

        try {
            WebDriverWait wait = new WebDriverWait(DriverUtil.getWebDriver(), 60);
            wait.until(ExpectedConditions.invisibilityOfElementLocated((By) locator));
        } catch (Exception e) {
            logger.info("There was an error " + e.getMessage());
        }
    }

    /**
     * Wait until element is disappear
     *
     * @param locator
     */
    public static void waitUntilInVisibilityOf(List<WebElement> locator)
    {
        try {
            WebDriverWait wait = new WebDriverWait(DriverUtil.getWebDriver(),waitTime );
            wait.until(ExpectedConditions.invisibilityOfAllElements(locator));
        } catch (Exception e) {
            logger.info("There was an error " + e.getMessage());
        }
    }


    /**
     * Wait until element is present in DOM
     *
     * @param locator, Seconds
     */
    public static void waitUntilPresenceOf(By locator,int seconds)
    {

        try
        {
            logger.info("Waiting for presence of  " + locator);
            WebDriverWait wait = new WebDriverWait(DriverUtil.getWebDriver(), seconds);
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
            logger.info("Done");
        }
        catch (Exception e)
        {
            logger.info("There was an error " + e.getMessage());
        }
    }

    /**
     * This method will clear cache of IE
     */
    public static void timeout(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (Exception ex)
        {
            logger.error("Error in waiting" + ex);
        }
    }

    /**
     * This method will select value from dropdown list
     * @param dropDown
     * @param value
     * @param selectionType
     */
    public static void selectFrommDropDown(WebElement dropDown, String value, String selectionType) {
        Select objDropDown = null;
        try {
            DriverUtil.waitUntilVisible(dropDown);
            moveToElement(dropDown);
            ClickWithJS(dropDown);
            timeout(300);
            objDropDown = new Select(dropDown);
            if (selectionType.equalsIgnoreCase("text")) {
                objDropDown.selectByVisibleText(value);
            } else if (selectionType.equalsIgnoreCase("index")) {
                objDropDown.selectByIndex(Integer.parseInt(value));
            } else if (selectionType.equalsIgnoreCase("value")) {
                objDropDown.selectByValue(value);
            } else {
                objDropDown.selectByIndex(1);
            }
        } catch (Exception ex)
        {
            logger.info("Error in selectFrommDropDown >> " + ex);
        }
    }

    /**
     * This method will select value from dropdown list
     * @param dropDown
     */
    public static boolean isOptionAvailabeInDropDown(WebElement dropDown, String option)
    {
        try
        {
            highLightElement(dropDown);
            return dropDown.getAttribute("innerHTML").contains(option);
        } catch (Exception ex)
        {
            logger.info("Error in DropDown option verification " + ex);
            return false;
        }
    }


    public static void  hoverElement(WebElement element)
    {
        try {
            highLightElement(element);
            Actions action = new Actions(DriverUtil.getWebDriver());
            action.moveToElement(element).build().perform();

        } catch (Exception ex)
        {
            logger.info("Error in hover element : " + ex.getMessage());
        }
    }


    public static void  hoverElement1(WebElement element)
    {
        try {
            highLightElement(element);
            Actions action = new Actions(DriverUtil.getWebDriver());
            action.moveToElement(element).build().perform();

            js = (JavascriptExecutor) DriverUtil.getWebDriver();
            js.executeScript("$('html, body').animate({scrollTop: $('#field_sku').offset().top-500}, 'slow');");



        } catch (Exception ex)
        {
            logger.info("Error in hover element : " + ex.getMessage());
        }
    }

    public static void  setZoom(int zoomPercentage)
    {
        try {
            js = (JavascriptExecutor) DriverUtil.getWebDriver();
            js.executeScript("document.body.style.zoom = '"+zoomPercentage+"%'");
        } catch (Exception ex)
        {
            logger.info("Error in hover element : " + ex.getMessage());
        }
    }



    public static void  moveToElement(WebElement element)
    {
        try {
            Actions action = new Actions(DriverUtil.getWebDriver());
            action.moveToElement(element).build().perform();

        } catch (Exception ex)
        {
            logger.info("Error in hover element : " + ex.getMessage());
        }
    }


    public static void  hoverClearSendKeys(WebElement element,String text) {
        try {
            highLightElement(element);
            element.clear();
            element.sendKeys(text);

        } catch (Exception ex) {
            logger.info("Error in hover element : " + ex.getMessage());
        }
    }



        public static void ClickWithJS(WebElement ele)
        {
            try
            {
             js.executeScript("arguments[0].click();", ele);
            }
            catch (Exception e)
            {
            }
        }

}

