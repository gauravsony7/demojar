package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {



    WebDriver driver;

    public HomePage()
    {
        driver=DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }


    @FindBy(xpath = "/html/body/header/nav/ul/li[1]/div/ul/li[4]/a")
    private WebElement womenMenu;


    @FindBy(xpath = "//*[@id='WomensBarComponent']/ul/li/div/div/div[1]/ul/li[1]/a")
    private WebElement womenSandalOp;

    @FindBy(xpath = "//*[@id='accountNameId']/div/a/span")
    private WebElement loginBtn;

    @FindBy(xpath = " //*[@id='authApp']/div/div[1]/div/a[1]")
    private WebElement signInLabel;




    public void selectCategoryUnderWomen()
    {
        System.out.println(" selectCategoryUnderWomen started ");
        DriverUtil.waitUntilClickable(womenMenu);
        BasePage.hoverElement(womenMenu);
        BasePage.timeout(100);
        waitAndClick(womenSandalOp);
        System.out.println("selectCategoryUnderWomen End");
    }

    public void clickSignInIcon()
    {
        waitAndClick(loginBtn);
        DriverUtil.waitUntilVisible(signInLabel);
    }


    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        BasePage.moveToElement(el);
        BasePage.ClickWithJS(el);
    }








































}
