package com.Pages;


import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;


public class CheckOutPage {
    private WebDriver driver;
    private ReadWriteExcelFile objExcel = new ReadWriteExcelFile();
    Random ran= new Random();


    public CheckOutPage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath  = "//*[@id='checkoutApp']/div/div/div/div/div[1]/div/a[1]/div[1]/p/span")
    private WebElement guestChekOutSection;

    @FindBy(xpath = ".//*[@type='submit' and normalize-space(.)='Checkout Now']")
    private WebElement ckeckOutNowBtn;

    @FindBy(id = "email")
    private  WebElement emailBox;

    @FindBy(id = "women")
    private  WebElement womenBox;

    @FindBy(id = "men")
    private  WebElement menBox;

    @FindBy(xpath = "//*[@id='checkoutApp']/div/div/div/div/div[2]/div[1]/div/form/button/span")
    private WebElement contineDelveryBtn;

    //*[@id="accordion-header--entry-0"]/div[1]/div


    @FindBy(id = "country")
    private  WebElement countrySelect;

    @FindBy(id = "titleCode")
    private  WebElement titleCodeSelect;

    @FindBy(id = "firstName")
    private  WebElement firstNameBox;

    @FindBy(id = "lastName")
    private  WebElement lastNameBox;

    @FindBy(id = "phone")
    private  WebElement phoneBox;

    @FindBy(id = "line1")
    private  WebElement line1Box;

    @FindBy(id = "line2")
    private  WebElement line2Box;

    @FindBy(id = "city")
    private  WebElement cityBox;

    @FindBy(id = "postalCode")
    private  WebElement postalCodeBox;

    @FindBy(xpath = "//*[@id='checkoutDeliveryForm']/div[2]/button")
    private WebElement contineBtn;

    @FindBy(xpath = "//*[@id='accordion-header--entry-0']/div[1]")
    private WebElement payOptSection;

    @FindBy(id = "cardNumber")
    private  WebElement cardNumber;

    @FindBy(id = "cc-exp-month")
    private  WebElement expmonth;

    @FindBy(id = "cc-exp-year")
    private  WebElement expyear;

    @FindBy(id = "securityNumber")
    private  WebElement securityNumber;

    @FindBy(id = "nameOnCard")
    private  WebElement nameOnCard;

    @FindBy(xpath = "//*[@id='cardPaymentForm']/div[2]/div[2]/label/span[1]/span")
    private WebElement useMyAddCheck;

    @FindBy(xpath = "//*[@id='cardPaymentForm']/div[3]/button/span")
    private WebElement continReviwOrdBtn;






    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        el.click();
        BasePage.highLightElement(el);
    }

    void waitAndSendkeys(WebElement el,String str)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        el.sendKeys(str);
    }



    public void fillEmailAddressa()
    {

            waitAndClick(guestChekOutSection);
            String email =ran.nextInt(3)+objExcel.getCellValue("emailAdress");
            waitAndSendkeys(emailBox,email);
            emailBox.sendKeys();
            waitAndClick(contineDelveryBtn);
    }

    public void fillPersonalInfo()
    {
        waitAndClick(firstNameBox);
        BasePage.selectFrommDropDown(countrySelect,objExcel.getCellValue("country"),"text");
        BasePage.selectFrommDropDown(titleCodeSelect,objExcel.getCellValue("Title"),"text");
        waitAndSendkeys(firstNameBox,objExcel.getCellValue("First name"));
        waitAndSendkeys(lastNameBox,objExcel.getCellValue("Last name"));
        waitAndSendkeys(phoneBox,objExcel.getCellValue("Phone number"));
        waitAndSendkeys(line1Box,objExcel.getCellValue("Address 1"));
        waitAndSendkeys(line2Box,objExcel.getCellValue("Address 2"));
        waitAndSendkeys(cityBox,objExcel.getCellValue("City"));
        waitAndSendkeys(postalCodeBox,objExcel.getCellValue("Postcode"));
        waitAndClick(contineBtn);
    }


    public void choosePaymentMethod()
    {
        waitAndClick(payOptSection);
        waitAndSendkeys(cardNumber,objExcel.getCellValue("Card number"));
        waitAndSendkeys(expmonth,objExcel.getCellValue("Expiry month"));
        waitAndSendkeys(expyear,objExcel.getCellValue("Expiry year"));
        waitAndSendkeys(securityNumber,objExcel.getCellValue("Security code"));
        waitAndSendkeys(nameOnCard,objExcel.getCellValue("Name on card"));
        waitAndClick(useMyAddCheck);
        BasePage.moveToElement(continReviwOrdBtn);
        BasePage.timeout(500);
        waitAndClick(continReviwOrdBtn);
    }


    public void completeCheckout()
      {
             fillEmailAddressa();
             fillPersonalInfo();
             choosePaymentMethod();
      }


















}


