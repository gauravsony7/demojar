package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import com.pmcretail.readwritedata.ReadWriteExcelFile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {



    private WebDriver driver;
    private ReadWriteExcelFile objExcel = new ReadWriteExcelFile();

    public RegistrationPage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(id = "titleCode")
    private  WebElement titleCodeSelect;

    @FindBy(id = "firstName")
    private  WebElement firstNameBox;

    @FindBy(id = "lastName")
    private  WebElement lastNameBox;

    @FindBy(id = "username")
    private  WebElement username;

    @FindBy(id = "password")
    private  WebElement password;

    @FindBy(id = "password2")
    private  WebElement password2;

    @FindBy(id = "day")
    private  WebElement day;

    @FindBy(id = "month")
    private  WebElement month;

    @FindBy(id = "year")
    private  WebElement year;

    @FindBy(id = "gender")
    private  WebElement gender;

    @FindBy(xpath = "//*[@id='authApp']/div/div[2]/div[1]/form/div[6]/div/div[1]/label/span[1]/span")
    private  WebElement privacyPolicyAgreement;

    @FindBy(xpath = "//*[@id='authApp']/div/div[2]/div[1]/form/button")
    private WebElement createAnAccountBtn;


    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        el.click();
        BasePage.highLightElement(el);
    }

    void waitAndSendkeys(WebElement el,String str)
    {
        DriverUtil.waitForJStoLoad();
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        el.sendKeys(str);
        DriverUtil.waitForJStoLoad();
    }


   public  void completeRegisration()
    {
        BasePage.selectFrommDropDown(titleCodeSelect,objExcel.getCellValue("Title"),"text");
        waitAndSendkeys(firstNameBox,objExcel.getCellValue("First name"));
        waitAndSendkeys(lastNameBox,objExcel.getCellValue("Last name"));
        // waitAndSendkeys(username,objExcel.getCellValue("emailAdress"));
        waitAndSendkeys(password,objExcel.getCellValue("password"));
        waitAndSendkeys(password2,objExcel.getCellValue("password"));
        BasePage.selectFrommDropDown(day,objExcel.getCellValue("Birth Date"),"text");
        BasePage.selectFrommDropDown(month,objExcel.getCellValue("Birth Month"),"text");
        BasePage.selectFrommDropDown(year,objExcel.getCellValue("Birth Year"),"text");
        BasePage.selectFrommDropDown(gender,objExcel.getCellValue("Gender"),"text");
        waitAndClick(privacyPolicyAgreement);
        waitAndClick(createAnAccountBtn);

        BasePage.timeout(30000);
    }



}
