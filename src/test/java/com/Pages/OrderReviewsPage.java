package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderReviewsPage
{

    @FindBy(xpath = "//*[@id=''checkoutApp']/div/div/div/div/div[1]/h1/span")
    private WebElement reviewOrderLabel;

    @FindBy(xpath = ".//span[text()='Place order']")
    private WebElement palceOrder;

    @FindBy(xpath = "//*[@id='change-delivery-method']/span")
    private WebElement changeDelBatn;

    @FindBy(xpath = " //*[@id='checkoutApp']/div/div/div/div/div[3]/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div[2]/fieldset/label[5]/span[1]")
    private WebElement checkEXPdelDel;

    @FindBy(xpath = "//*[@id='checkoutApp']/div/div/div/div/div[3]/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div[2]/fieldset/label[1]/span[1]/span")
    private WebElement checkFreePdelDel;

    @FindBy(xpath = "//*[@id='checkoutApp']/div/div/div/div/div[3]/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div[2]/button/span")
    private WebElement confirmBth;


    By placeOrderXpath = By.xpath(".//span[text()='Place order']");



    private WebDriver driver;

    public OrderReviewsPage() {
         this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }
    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        BasePage.moveToElement(el);
        el.click();

    }
   public void clickOnPlaceOrderbtn()
    {
        BasePage.waitUntilPresenceOf(placeOrderXpath,60);
        BasePage.moveToElement(palceOrder);
        BasePage.timeout(500);
        palceOrder.click();
    }

   public void changeDelivery(String delTypr)
    {
        waitAndClick(changeDelBatn);
        if(delTypr.equalsIgnoreCase("free"))
            waitAndClick(checkFreePdelDel);
        else
            waitAndClick(checkEXPdelDel);

        waitAndClick(confirmBth);
    }




}
