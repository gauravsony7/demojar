package com.Pages;

import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmationPage {

    private WebDriver driver;


    public ConfirmationPage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = ".//div[@class='show-for-large-up']//*[@aria-label='Create account']")
    private WebElement cretaeAccBtn;


    public void clickOnCreateAccountLink()
    {
        DriverUtil.waitUntilVisible(cretaeAccBtn);
        cretaeAccBtn.click();
    }


    public void waitforCreateAccountLink()
    {
        DriverUtil.waitUntilVisible(cretaeAccBtn);
    }



}
