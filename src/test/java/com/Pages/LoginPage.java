package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {

    private WebDriver driver;

    public LoginPage()
    {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = " //*[@id='authApp']/div/div[1]/div/a[1]")
    private WebElement signInLabel;

    @FindBy(xpath = "//*[@id='authApp']/div/div[1]/div/a[2]/div[1]")
    private WebElement creatAccLabel;

    @FindBy(name = "username")
    private WebElement inUserName;

    @FindBy(name = "password")
    private WebElement inPassword;

    @FindBy(xpath = "//*[@id=\"authApp\"]/div/div[2]/div/form/button")
    private WebElement loginBtn;


    @FindBy(id = "my-account-page")
    private WebElement userNameLabel;



    public void enterUserName(String userName)
    {
        DriverUtil.waitUntilVisible(inUserName);
        BasePage.hoverClearSendKeys(inUserName,userName);

    }

    public void enterPassword(String passWord)
    {
        DriverUtil.waitUntilVisible(inPassword);
        BasePage.hoverClearSendKeys(inPassword,passWord);
    }

    public void clickOnSIgnIn()
    {
        DriverUtil.waitUntilVisible(loginBtn);
        loginBtn.click();
        DriverUtil.waitUntilVisible(userNameLabel);
    }

    public void clickOnLoginSection()
    {
        DriverUtil.waitUntilVisible(signInLabel);
        signInLabel.click();
        DriverUtil.waitUntilVisible(inUserName);
    }

    public void login(String userName , String password)
    {
        clickOnLoginSection();
        enterUserName(userName);
        enterPassword(password);
        clickOnSIgnIn();
    }











}
