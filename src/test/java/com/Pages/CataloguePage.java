package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CataloguePage
{
    private WebDriver driver;


    public CataloguePage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }


    @FindBy(xpath = "//*[@id='plp']/div/div[1]/div/div/div/div[3]/div[3]/div/div/div[1]/div/a/div[1]/picture/img")
    private WebElement womenSandalPrd;



    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        el.click();
    }

    public void clickONproduct()
    {
        System.out.println(" clickONproduct started ");
        waitAndClick(womenSandalPrd);
        System.out.println(" clickONproduct end ");
    }


}
