package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ProductDetailPage
{
    private WebDriver driver;

    public ProductDetailPage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);
    }



    @FindBy(xpath = "//*[@id='pdpDetails']/div[1]/div/div[1]/div/div/div[1]/div/div/picture/img")
    private WebElement womenSandalPrdDEatil;


    @FindBy(xpath = "//*[@id='pdpDetails']/div[1]/div/div[2]/div/div[3]/div[1]/div[2]/button[7]")
    private WebElement PrdDEatilSize;


    @FindBy(xpath = "//*[@id='pdpDetails']/div[1]/div/div[2]/div/div[3]/div[2]/div/div[1]/button")
    private WebElement AddToCAt;

    @FindBy(xpath = "//*[@id='mini-cart']/div/a")
    private WebElement cartIcone;


    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/div/h2/span")
    private WebElement orderSummary;

    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[4]/div/button")
    private WebElement checkOutBtn;




    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        el.click();
    }



    public void addProductToCart()
    {
        System.out.println(" addProductToCart started ");
        DriverUtil.waitUntilClickable(womenSandalPrdDEatil);
        waitAndClick(PrdDEatilSize);
        waitAndClick(AddToCAt);
        waitAndClick(cartIcone);
        System.out.println(" addProductToCart end ");
    }


}
