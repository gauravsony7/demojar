package com.Pages;

import com.Util.BasePage;
import com.pmcretail.driver.DriverUtil;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CartPage
{
    private WebDriver driver;

    public CartPage() {
        this.driver = DriverUtil.getWebDriver();
        PageFactory.initElements(this.driver, this);

    }


    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/div/h2/span")
    private WebElement orderSummary;

    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[4]/div/button")
    private WebElement checkOutBtn;

    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[3]/div/form/div/button/span")
    private WebElement applyPromoBtn;

    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[3]/div/button/span")
    private WebElement havePromoLbl;

    @FindBy(xpath = "//*[@id='cart']/div/div[2]/div/div[2]/div/div/div[2]/div/div[3]/div/form/div/div/input")
    private WebElement PromoIn;

    @FindBy(xpath = ".//span[@class='cart-order-summary__promo-code']")
    private WebElement alredayAppliedPromoLbl;


    @FindBy(xpath = "//*[@id='checkoutApp']/div/div/div/div/div[2]/div[1]/div/form/div[2]/div/p[2]/span")
    private WebElement errorMsgWrongPromo;

    @FindBy(xpath  = "//*[@id='checkoutApp']/div/div/div/div/div[1]/div/a[1]/div[1]/p/span")
    private WebElement guestChekOutSection;



    void waitAndClick(WebElement el)
    {
        DriverUtil.waitUntilClickable(el);
        BasePage.highLightElement(el);
        el.click();
    }



    public void waitForOrderSummaryPage()
    {
        System.out.println(" waitForOrderSummaryPage started ");
        DriverUtil.waitUntilVisible(orderSummary);
        waitAndClick(orderSummary);
    }

    public void applyPromocode(String promo)
    {
        System.out.println("apply Promo code started");
        DriverUtil.waitUntilVisible(orderSummary);
        waitAndClick(havePromoLbl);
        // BasePage.timeout(100);
        waitAndClick(PromoIn);
        PromoIn.sendKeys(promo);
        // BasePage.timeout(100);
        waitAndClick(applyPromoBtn);
    }

    public void clickOnCheckout()
    {
        waitAndClick(checkOutBtn);
        DriverUtil.waitForJStoLoad();
        DriverUtil.getWebDriver().navigate().refresh();
        DriverUtil.waitForJStoLoad();
        DriverUtil.waitUntilVisible(guestChekOutSection);
    }


    public boolean verifyErrorMessgeforWrongPromoCode(String expectedError)
    {
        String actualError;
        try
        {
            actualError= errorMsgWrongPromo.getText();
            BasePage.highLightElement(errorMsgWrongPromo,"green");
        }
        catch (NoSuchElementException e)
        {
            return false;
        }

        System.out.println("Actual Error Message : "+actualError);
        System.out.println("Expected Error Message : "+expectedError);

        return actualError.equalsIgnoreCase(expectedError);
    }



}
